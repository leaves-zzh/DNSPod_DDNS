DNSPod_DDNS
===========

Thanks for lixin-dnspod-ddns

##说明
该脚本使用`python`编写，利用`dnspod api`开发的一个ddns

##使用
####1、编辑文件

编辑DNSPod.py,填写`public_dic["login_email"]、public_dic["login_password"]、domain_name、record_name`对应参数

`public_dic["login_email"]=""` 你在dnspod.cn上的账号

`public_dic["login_password"]=""` 你在dnspod.cn上的密码

`domain_name={"","",""}` 你要操作的域名，例如 baidu.com、google.com

`record_name={"@","*","www"}` 你要解析的记录，默认@、*、www

####2、运行命令

`python2 DNSPod_DDNS.py` or `python DNSPod_DDNS.py`

##日志

`cache_dir="/var/cache/dnspod"` 默认日志文件地址，如果不是root，请更改为有读写权限的目录

另附上Archlinux的Systemd .service 使其开机启用 
