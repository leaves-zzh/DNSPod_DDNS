﻿#!/usr/bin/env python
#-*- coding:utf-8 -*-
import sys
import urllib2
import urllib
import json
import time
import socket
import os

public_dic={}
public_dic["login_email"]="" #replace your email 替换你的dnspod账号email
public_dic["login_password"]="" #replace your password 替换你的密码
domain="" 
domain_name=["","",""] #replace your domain  你的域名
record="" 
record_name=["@","*","www"] #replace your record 你的二级域名
public_dic["format"]="json"
headers={}
headers["User-Agent"]=""

ip=''
sleepTime=3000
cache_dir="/var/cache/dnspod"

def WriteLog(msg):
    f=open(cache_dir+'/ddns.log','a')
    f.write(time.strftime("%Y/%m/%d %H:%M:%S",time.localtime())+"    "+msg+"\n")
    f.close()
def getDomainID():
    url="https://dnsapi.cn/Domain.Info"
    params=public_dic.copy()
    params["domain"]=domain
    req=urllib2.Request(url,headers=headers,data=urllib.urlencode(params))
    resp=urllib2.urlopen(req)
    formatJson=json.load(resp)
    if formatJson["status"]["code"]!="1":
        WriteLog("getDomainID has Error: ("+formatJson["status"]["code"]+")"+formatJson["status"]["message"])
        return 0
    else:
        return formatJson["domain"]["id"]
    pass

def getRecordID(domain_id):
    url="https://dnsapi.cn/Record.List"
    params=public_dic.copy()
    params["domain_id"]=domain_id
    params["sub_domain"]=record
    req=urllib2.Request(url,headers=headers,data=urllib.urlencode(params))
    resp=urllib2.urlopen(req)
    myJson=json.load(resp)
    if myJson["status"]["code"]!="1":
        WriteLog("getRecordID has Error: ("+formatJson["status"]["code"]+")"+formatJson["status"]["message"])
        return 0
    else:
        ip=myJson["records"][0]['value']
        return myJson["records"][0]['id']
    pass
def getMyIp():
    url="ns1.dnspod.net"
    port=6666
    mySocket=socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
    mySocket.connect((url,port))
    recv=mySocket.recv(16) 
    mySocket.close()
    return recv

    pass
def setDDNS(domainID,recordID):
    url="https://dnsapi.cn/Record.Ddns"
    params=public_dic.copy()
    params["domain_id"]=domainID
    params["sub_domain"]=record
    params["record_id"]=recordID
    params["record_line"]="默认"
    req=urllib2.Request(url,headers=headers,data=urllib.urlencode(params))
    resp=urllib2.urlopen(req)
    myJson=json.load(resp)
    if myJson["status"]["code"]!="1":
        WriteLog("setDDNS has Error: ("+myJson["status"]["code"]+")"+myJson["status"]["message"])
    pass
    
if __name__ == '__main__':
    while True:
        try:
            newIP=getMyIp()
            if ip !=newIP:
                for domain in domain_name:
                    domainID=getDomainID()
                    if domainID!=0:
                        for record in record_name:        
                            recordID=getRecordID(domainID)
                            if recordID !=0:
                                setDDNS(domainID,recordID)
                                ip=newIP
                                WriteLog("new ip="+str(ip))
            pass
        except Exception, e:
            WriteLog("has a ERROR:"+str(e.strerror))
            pass
        time.sleep(sleepTime)
    pass
    
